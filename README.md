#### Virtual environment setup
For creating virtual environment with python 3.6 in your machine type this
```
virtualenv -p python3.6 your_envname
```
Activate virtualenv
```
. path/to/virtualenv/your_envname/bin/activate
```
Install dependencies from requirements.txt
```
pip install -r requirements.txt
```
Make migrations and create superusers
```
python manage,py makemigrations
python manage.py migrate
python manage.py createsuperuser
python manage.py runserver
```

Now. go to `hottp://localhost:8000/` to see the list of todos.
you can list and create todo from here. To see the details and/or update/delete an 
specific item, go to `http://localhost:8000/<id>/` where `id` is the todo's id.

To view from browser, open [this](index.html) file in a browser. To add a work, enter the name in the input box and click Add button. To mark it as done/not done, click on the task. To delete, click the `X` button.

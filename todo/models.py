from django.db import models


class Todo(models.Model):
    name = models.CharField(max_length=500, null=True, blank=True)  # name of work
    done = models.BooleanField(default=False)  # work is done or not
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.id) + ' ' + str(self.name)

    # class Meta:
    #     ordering = ('-created_at',)

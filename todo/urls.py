from django.urls import path

from .views import TodoListCreateAPIView, TodoRetrieveUpdateDestryAPIView

app_name = 'client'

urlpatterns = [
    path('', TodoListCreateAPIView.as_view(), name='todos'),
    path('<int:pk>/', TodoRetrieveUpdateDestryAPIView.as_view(), name='todos'),
]
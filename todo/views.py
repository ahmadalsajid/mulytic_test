from rest_framework import generics
from .serializers import TodoSerializer
from .models import Todo


class TodoListCreateAPIView(generics.ListCreateAPIView):
    queryset = Todo.objects.all()
    serializer_class = TodoSerializer


class TodoRetrieveUpdateDestryAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Todo.objects.all()
    serializer_class = TodoSerializer

    # def get_object(self):
    #     obj = generics.get_object_or_404(Todo, id=self.kwargs.get("id"))
    #     return obj
